var app = angular.module("myApp", []);

app.controller('FileController', ['$scope', 'lineSplitterService','splitterService','$rootScope', function($scope, ls, ss,$rootScope){ 
$scope.openFile = function(event) {
  var input = event.target;

  var reader = new FileReader();
  reader.onload = function() {
    var text = reader.result;
    var s = $scope.print(text)
    //alert(s)
    localStorage.setItem('data',s);
    
  };
  reader.readAsText(input.files[0]);
};

 
$scope.print = function(x) {

    console.log(x)
  var invoiceLines = ls.lineSplitter(x);
  console.log(invoiceLines)

  var numbers = invoiceLines.map(function(line) {
   
    var splits0 = ss.split(line[0]);
    var splits1 = ss.split(line[1]);
    var splits2 = ss.split(line[2]);
    var numberStr = "";
    for (let i = 0; i < splits0.length; i++) {
      if (splits0[i] == "   ") {
        //possble 1 or 4
        if (splits1[i] == "|_|") {
          numberStr += 4;
        } else {
          numberStr += 1;
        }
      } else if (splits0[i] == " _ ") {
        //possible 2,3,5,6,7,8,9,0
        if (splits1[i] == "  |") {
          //possible 7
          numberStr += 7
        } else if (splits1[i] == " _|") {
          //possible 2,3
          if (splits2[i] == "|_ ") {
            numberStr += 2;
          } else {
            numberStr += 3;
          }
        } else if (splits1[i] == "|_ ") {
          //possible 5,6
          if (splits2[i] == "|_|") {
            numberStr += 6;
          } else {
            numberStr += 5;
          }
        } else if (splits1[i] == "|_|") {
          //possible 8,9
          if (splits2[i] == "|_|") {
            numberStr += 8;
          } else {
            numberStr += 9;
          }
        } else {
          numberStr += 0;
        }
      }

    }
    console.log(numberStr)
    return numberStr;

  });
  console.log(numbers)
  result = numbers;
  return numbers;

}


function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}


document.getElementById("dwn-btn").addEventListener("click", function(){
    var text = localStorage.getItem('data');
    var filename = "output_user_story1.txt";
    
    download(filename, text);
}, false);
   
}]);


app.service('lineSplitterService', function() {
    return {
            lineSplitter:function(str) {
              console.log(str)
            var arr = [];
            var lines = str.split('\n');
            lines = lines.filter(function(x) {
              
              return x !== "";
            })
            console.log(lines)
            for (let i = 0; i < lines.length; i += 3) {
              console.log([lines[i], lines[i + 1], lines[i + 2]])
              arr.push([lines[i], lines[i + 1], lines[i + 2]]);
              console.log(arr)
            }
            return arr;
}
        }
      });

app.service('splitterService', function() {
    return {
            split:function(str) {
            var arr = [];
            for (let i = 0; i < str.length; i += 3) {
              if (str.charAt(i) == '\n') 
                  break;
              arr.push(str.slice(i, i + 3));
            }
            return arr;
          }
        }
      });

